/*jslint node: true, plusplus: true, sloppy: true, white: true*/
var app = angular.module('mohio', ['ngMaterial']);

app.controller('AppCtrl', ['$scope', '$http', '$location', '$mdDialog', '$mdToast', function($scope, $http, $location, $mdDialog, $mdToast){

  $scope.init = function(userid){
    $scope.userid = userid;

    //Get Notes
    $http.get('users/' + $scope.userid + '/notes')
      .success(function(response){
      $scope.notes = response;
    });
  };


  $scope.controlPanelOpen = false;
  $scope.toggleControlPanel = function(){
    $scope.controlPanelOpen = ! $scope.controlPanelOpen;
  };

  $scope.register = function(){
    window.location.href = '/register';
  };

  $scope.login = function(){
    //    window.location.href = '/auth/facebook';
    window.location.href = '/login';
  };

  $scope.logout = function(){
    window.location.href = '/logout';
  };

  $scope.addNote = function() {
    $http.post("/notes", {text:$scope.note, userid:$scope.userid})
      .success(function(response){
      //      console.log(response);
      $scope.notes.push(response);
      $scope.note = '';
      $scope.showSimpleToast('Note added successfully');
    });
  };

  $scope.destroyNote = function(noteid) {
    $http.post('/notes/' + noteid + '/destroy')
      .success(function(response){
      for (index = 0; index < $scope.notes.length; ++index){
        if ($scope.notes[index].id === response[0].id){
          $scope.notes.splice(index,1);
          $scope.showSimpleToast('Note deleted successfully');
          break;
        }
      }
    });
  };

  $scope.showSimpleToast = function(message){
    $mdToast.show(
      $mdToast.simple()
      .content(message)
      .hideDelay(3000)
    );
  };

  $scope.isAdmin = ($scope.userid === 1 || $scope.userid === 3);

  //    function(){
  //    var ids = [1,3, null];
  //    ids.forEach(function(id){
  //      if (id === null){
  //        return false;
  //      };
  //      if (id === $scope.userid){
  //        return true;
  //      };
  //    });
  //  };






}]);


app.config(function($mdThemingProvider) {

  $mdThemingProvider.definePalette('cardinal', {
    '50': '000500',
    '100': '003800',
    '200': '005200',
    '300': '036b00',
    '400': '1d850d',
    '500': '369e26',
    '600': '43AB33', //This is the green color
    '700': '50b840',
    '800': '69d159',
    '900': '83eb73',
    'A100': '9cff8c',
    'A200': 'b6ffa6',
    'A400': 'e9ffd9',
    'A700': 'ffffff',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
    // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                           '50', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });

  $mdThemingProvider.definePalette('gold', {
    '50': '592600',
    '100': '8c5900',
    '200': 'a67300',
    '300': 'bf8c00',
    '400': 'd9a600',
    '500': 'f2bf00',
    '600': 'ffcc00', //This is the gold color
    '700': 'ffd90d',
    '800': 'fff226',
    '900': 'ffff40',
    'A100': 'ffff59',
    'A200': 'ffff73',
    'A400': 'ffffa6',
    'A700': 'ffffff',
    'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
    // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                           '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });



  $mdThemingProvider.theme('default')
    .primaryPalette('cardinal', {
    'default': '600',
    'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
    'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
    'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
  })
  // If you specify less than all of the keys, it will inherit from the
  // default shades
    .accentPalette('gold', {
    'default': '600' // use shade 200 for default, and keep all other shades the same
  });
});