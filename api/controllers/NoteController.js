/*jslint node: true, sloppy: true, white: true*/
/**
 * NoteController
 *
 * @description :: Server-side logic for managing notes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  
  //POST /notes
  createNote: function (req, res){
    var userId = req.session.passport.user;
    console.log("User ID: " + userId);
    userId = req.body.userid; //Both work
    console.log("User ID: " + userId);
    var note = req.body.text;
    
    Note.create({text:note}).then(function(note){
      note.users.add(userId);
      note.save(console.log);
      res.json(note);
    });
  },
  
  //POST /notes/:noteid/destroy
  destroyNote: function (req, res){
    var noteid = req.params.noteid;
    Note.destroy(noteid).then(function(note){
      console.log('The record has been deleted');
      res.json(note);
    });
  },
  
  getTags: function (req, res){
    var noteid = req.params.noteid;
    //Do something if no note is found (can't populate then)
    Note.findOne(noteid).populate('tags').then(function(note){
      res.json(note.tags);
    });
  }


};

