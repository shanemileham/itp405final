/**
 * TagController
 *
 * @description :: Server-side logic for managing tags
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  getNotes: function (req, res){
    var tagid = req.params.tagid;
    Tag.findOne(tagid).populate('notes').then(function(tag){
      res.json(tag.notes);
    });
  }
};

