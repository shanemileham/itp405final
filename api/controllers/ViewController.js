/*jslint node: true, sloppy: true, white: true*/
/**
 * ViewController
 */

module.exports = {
  
  mainPage: function (req, res){
//    console.log(req.user);
    if (req.user == null){
      console.log("Not logged in");
      res.render('infopage');
    }else{
      res.render('dashboard', { user: req.user });
    }
  },
  
  test: function(req, res){
//    res.json(req);
    console.log(req.session.passport.user);
  },


};

