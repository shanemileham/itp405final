/*jslint node: true, sloppy: true, white: true*/
/**
 * isAdmin
 */
module.exports = function(req, res, next) {

  var ids = [1,3];
  ids.forEach(function(id){
    if (id === req.session.passport.user){
      return next();
    }
  });

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
//  return res.forbidden('You are not permitted to perform this action.');
};
